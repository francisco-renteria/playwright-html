//Traduciion del estado
export function traducirEstado(estado: string): string {
    switch (estado) {
      case "passed":
        return "Aprobado";
      case "failed":
        return "Fallido";
      case "timedOut":
        return "Tiempo agotado";
      case "skipped":
        return "Omitido";
      case "interrupted":
        return "Interrumpido";
      default:
        return "Desconocido";
    }
  }
  