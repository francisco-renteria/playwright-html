// $(document).ready(function () {
//     $('#test-list-table').DataTable(
//         {
//             //responsive: true,
//             //dom: 'Qfrtip',
//             //select: true
//         }
//     );
// });
$(document).ready(function () {
  // Agregar un controlador de eventos al botón para mostrar/ocultar .copy-button
  $("#toggle-button").on("click", function () {
    $(".copy-button").toggle();
  });
});

$(document).ready(function () {
  var $table = $("#test-list-table2").DataTable({
    paging: false, // Deshabilita la paginación
    ordering: false, // Deshabilita la ordenación
  });

  var previousValue = null;
  var rowspan = 1;

  $table.rows().every(function (rowIdx, tableLoop, rowLoop) {
    var data = this.data();
    var currentValue = data[0];

    if (previousValue === null) {
      previousValue = currentValue;
    } else if (currentValue !== previousValue) {
      $table
        .cell({ row: rowIdx - rowspan, column: 0 })
        .data(previousValue)
        .node()
        .setAttribute("rowspan", rowspan);
      rowspan = 1;
      previousValue = currentValue;
    } else {
      $table.cell({ row: rowIdx, column: 0 }).node().style.display = "none"; // Oculta la celda
      rowspan++;
    }
  });
  if (0)
    $table
      .cell({ row: $table.rows().count() - rowspan, column: 0 })
      .data(previousValue)
      .node()
      .setAttribute("rowspan", rowspan);
  else {
    $table
      .cell({ row: $table.rows().count() - rowspan, column: 0 })
      .data(previousValue)
      .node()
      .setAttribute("rowspan", rowspan);
    // Selecciona todas las celdas en la primera columna de la tabla
    // Selecciona todas las celdas en la primera columna de la tabla
    var $firstColumnCells = $table.rows().nodes().to$().find("td:first-child");

    let blocked = 0;

    $firstColumnCells.each(function () {
      var cell = $table.cell(this);
      var existingData = cell.data(); // Obtener la data existente

      // Agregar el botón solo si no contiene ya un botón y no está bloqueado
      if (blocked === 0) {
        var newData =
          existingData +
          '<br><button class="btn btn-outline-dark btn-sm copy-button">Img to clipboard</button>'; // Agregar el nuevo contenido al final
        cell.data(newData); // Actualizar el contenido de la celda
        blocked = cell.node().rowSpan - 1;
      } else {
        blocked--; // Restar uno a blocked para indicar que una celda ha sido bloqueada
      }
    });

    // Redibujar la tabla (si es necesario)
    $table.draw();
  }
});
function cloneUpToNDepth($element, n) {
  if (n === 0) {
    return $element[0].cloneNode(); // Clonar el elemento sin sus hijos
  } else {
    var $clonedElement = $element[0].cloneNode(); // Clonar el elemento sin hijos
    $element.children().each(function () {
      $clonedChild = cloneUpToNDepth($(this), n - 1);
      $clonedElement.append($clonedChild);
    });
    return $clonedElement;
  }
}
$(document).ready(function () {
  $(".copy-button").on("click", function () {
    // Ocultar todos los botones "copy"
    $(".copy-button").hide();
    console.time("60rem");

    const $table = $("#test-list-table2");
    const $buttonCell = $(this).parent(); // .closest("td"); // Obtener la celda que contiene el botón
    const $buttonRow = $buttonCell.parent(); //.closest("tr"); // Obtener la fila que contiene la celda con el botón
    const rowspan = $buttonCell.prop("rowspan"); // Obtener el valor del rowspan
    // Redimensionar la tabla a width=60rem
    $table.width("60rem");
    console.timeEnd("60rem");
    console.time("clonado");
    // Capturar solo las filas relevantes (la fila actual y las siguientes n filas)
    const $rowsToCopy = $buttonRow
      .nextAll(":lt(" + (rowspan - 1) + ")")
      .addBack();
    console.log($rowsToCopy);

    // Crear un elemento temporal para capturar
    const $tempContainer = $("<div class='deleteme'>").appendTo("body");
    // const $tempTable = $(cloneUpToNDepth($table,5));
    // console.log($tempTable)
    const $tempTable = $table.clone();
    $tempTable.find("td div").remove();
    console.log("TABLA", $tempTable);
    $tempContainer.css("width", "fit-content");
    // Ocultar las últimas dos columnas en el thead
    console.log("tHH", $tempTable.find("thead th"));
    $tempTable.find("thead th:gt(-3)").css("display", "none");
    $tempTable.find("tbody").empty();
    $tempTable.find("tbody").append($rowsToCopy.clone());

    $tempTable.find(".TDClassesCss3").remove();
    $tempTable.find("div").remove();
    console.log("TABLASINDIV", $tempTable);
    $tempContainer.append($tempTable);

    console.timeEnd("clonado");
    console.time("Acabando");
    // Capturar el elemento temporal con html2canvas
    domtoimage
      .toPng($(".deleteme")[0], { bgcolor: "white" })
      .then(function (canvas) {
        // Obtiene el contexto de 2D del canvas
        //const context = canvas.getContext("2d");
        // Extrae la imagen en formato base64
        const dataURL = canvas.split(",")[1];
        //console.log(dataURL);
        console.timeEnd("Acabando");
        // Eliminar el elemento temporal
        $tempContainer.remove();

        // Llama a la función para copiar la imagen en formato base64 al portapapeles
        copyBase64ImageToClipboard(
          dataURL,
          () => {
            console.log(
              "Imagen en formato base64 copiada al portapapeles con éxito."
            );
            // Mostrar nuevamente todos los botones "copy"
            $(".copy-button").show();
            // Restaurar el ancho original de la tabla
            $table.css("width", "");
          },
          (err) => {
            console.error(
              "Error al copiar la imagen en formato base64 al portapapeles:",
              err
            );
            // Mostrar nuevamente todos los botones "copy" en caso de error
            $(".copy-button").show();
            // Restaurar el ancho original de la tabla
            $table.css("width", "");
          }
        );
      });
  });
});

// Función para copiar una imagen en formato base64 al portapapeles
function copyBase64ImageToClipboard(base64Image, onDone, onError) {
  // Convierte la base64 en Blob
  const byteCharacters = atob(base64Image);
  const byteNumbers = new Array(byteCharacters.length);
  for (let i = 0; i < byteCharacters.length; i++) {
    byteNumbers[i] = byteCharacters.charCodeAt(i);
  }
  const byteArray = new Uint8Array(byteNumbers);
  const blob = new Blob([byteArray], { type: "image/png" });

  // Copia el Blob al portapapeles
  const data = [new ClipboardItem({ [blob.type]: blob })];

  navigator.clipboard.write(data).then(
    () => {
      onDone();
    },
    (err) => {
      onError(err);
    }
  );
}
